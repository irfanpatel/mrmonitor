package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/streadway/amqp"
	m "gitlab.com/irfanpatel/mrmonitor/models"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http"
)

var collection *mongo.Collection
var queue amqp.Queue
var channel *amqp.Channel

func LogsCollection(c *mongo.Database){
	collection = c.Collection("logs")
}

func AmqpChannel(q amqp.Queue, ch *amqp.Channel) {
	queue = q
	channel = ch

}
func AddLog(c *gin.Context){
	var log m.Logdata
	//fmt.Println(c.PostForm("destinationip"))
	c.BindJSON(&log)
	messageBody := new(bytes.Buffer)
    json.NewEncoder(messageBody).Encode(log)


	//message, err := collection.InsertOne(context.TODO(),log)
	//fmt.Println(log)
    //fmt.Println(message.InsertedID)
    err := channel.Publish(
    	"",
    	queue.Name,
    	false,
    	false,
    	amqp.Publishing {
    		ContentType:"application/json",
    		Body: messageBody.Bytes(),
		})

	if err != nil {
		fmt.Println("error inserting log data")
		c.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"message": "Something went wrong",
		})

		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"status":  http.StatusCreated,
		"message": "Log added successfully",
	})
	return
}