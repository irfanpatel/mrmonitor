package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/irfanpatel/mrmonitor/config"
	"gitlab.com/irfanpatel/mrmonitor/routes"
	"log"
)

func main(){

	config.ConnectToQueue()

	router := gin.Default()

	routes.Routes(router)

	log.Fatal(router.Run())
}
