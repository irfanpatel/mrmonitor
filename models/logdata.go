package models

type Logdata struct {
	Datetime        string `json:"datetime"`
	Action          string `json:"action"`
	Protocol        string `json:"protocol"`
	SourceIP        string `json:"source_ip"`
	DNSName         string `json:DNSName`
	DestinationIP   string `json:"destinationip"`
	SourcePort      string `json:"source_port"`
	DestinationPort string `json:"destination_port"`
	MessageSize     string `json:"message_size"`
	Path            string `json:"path"`
}


