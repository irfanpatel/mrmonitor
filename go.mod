module gitlab.com/irfanpatel/mrmonitor

go 1.13

require (
	github.com/gin-gonic/gin v1.6.2
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	go.mongodb.org/mongo-driver v1.3.2
)
