package config

import (
	"context"
	"github.com/streadway/amqp"
	"gitlab.com/irfanpatel/mrmonitor/controllers"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"time"
)


func Connect() {
	// Database Config
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.NewClient(clientOptions)

	//Set up a context required by mongo.Connect
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)

	//To close the connection at the end
	defer cancel()

	err = client.Ping(context.Background(), readpref.Primary())
	if err != nil {
		log.Fatal("Couldn't connect to the database", err)
	} else {
		log.Println("Connected!")
	}
	db := client.Database("mrmonitor")
    controllers.LogsCollection(db)
	return
}

func ConnectToQueue() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	failOnError(err,"Error connecting to RabbitMQ ")



	ch, err := conn.Channel()
	failOnError(err, "Error opening channel")


    q, err := ch.QueueDeclare(
    	"logs",
    	 false,
    	 false,
    	 false,
    	 false,
    	 nil,
	)
   failOnError(err,"Failed to declare logs queue")

	_ = ch.Publish(
		"",
		q.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte("Hello"),
		})

   controllers.AmqpChannel(q,ch)

}

func failOnError(err error, msg string){
	if err != nil {
		log.Fatalf("%s %s", msg, err)
	}
}